import unidecode


def fetch_specialties_by_code_ref(code_ref, csv_file_path):
    # Lire le fichier CSV
    df = pd.read_csv(csv_file_path, delimiter=';')

    # Assurez-vous que la colonne 'code_de_ref' et 'code_savoirfaire' existent
    if 'code_de_ref' not in df.columns or 'code_savoirfaire' not in df.columns:
        raise ValueError("Le fichier CSV doit contenir les colonnes 'code_de_ref' et 'code_savoirfaire'")

    # Filtrer les lignes où 'code_de_ref' correspond au code_ref donné
    filtered_df = df[df['code_de_ref'] == code_ref]

    # Obtenir la liste des 'code_savoirfaire'
    code_savoirfaire_list = filtered_df['code_savoirfaire'].tolist()

    return code_savoirfaire_list


# coalesce(multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb), '')
def str_normalization(string_param: str):
    return unidecode.unidecode(string_param.lower().strip()).replace('-', '').replace(' ', '').replace("'", '').replace('"', '')
