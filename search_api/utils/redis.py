import json
import hashlib
from functools import wraps
from fastapi.responses import JSONResponse


import asyncio
from redis import asyncio as aioredis

from starlette.middleware.base import BaseHTTPMiddleware
from starlette.responses import Response

from config.config import settings

cache_time = settings.CACHE_TIME
redis_url = settings.REDIS_URL

# Redis connection setup
redis = aioredis.from_url(redis_url, decode_responses=True)

class CacheMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        # Create a unique cache key based on the request
        cache_key = f"cache:{hashlib.md5(str(request.url).encode()).hexdigest()}"

        # Check if the response is cached
        cached_response = await redis.get(cache_key)
        if cached_response:
            return Response(cached_response, media_type="application/json")

        # If not cached, process the request
        response = await call_next(request)

        # Read the response body explicitly
        response_body = b""
        async for chunk in response.body_iterator:
            response_body += chunk


        # Cache the response if it’s successful
        if response.status_code == 200:
            await redis.setex(cache_key, cache_time, response_body.decode())  # Cache for 10 minutes

        return Response(response_body, status_code=response.status_code, headers=dict(response.headers))

def cache_response():
    def decorator(func):
        @wraps(func)
        async def wrapper(*args, **kwargs):
            # Generate cache key based on function and arguments
            cache_key = f"cache:{func.__name__}:{args}:{kwargs}"
            cached_data = await redis.get(cache_key)

            if cached_data:
                value_as_str = cached_data.decode("utf-8")
                value = json.loads(value_json)
            else:

                # Call the original function if not cached
                value = await func(*args, **kwargs)
                # Cache the response
                value_as_str = json.dumps(value)
                await redis.setex(cache_key, cache_time, value_as_str)
            return value 
        return wrapper
    return decorator

