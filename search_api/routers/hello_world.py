from fastapi import APIRouter
from starlette import status
from starlette.requests import Request

router = APIRouter()

@router.get("/", status_code=status.HTTP_200_OK)
async def get_hello_world(request: Request) -> dict:
    return {"message": "Hello, explore our documentation at " + str(request.url) + "docs"}

