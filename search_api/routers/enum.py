import json
from fastapi import APIRouter
from starlette import status
from models.enum import ProfessionModel, SpecialityModel
from utils.redis import cache_response

router = APIRouter()


@router.get("/specialities", status_code=status.HTTP_200_OK)
async def get_specialities():
    enum_dict = {speciality.name: speciality.value for speciality in SpecialityModel}
    json_data = json.dumps(enum_dict, indent=2)
    return json_data

@router.get("/professions", status_code=status.HTTP_200_OK)
def get_professions():
    enum_dict = {profession.name: profession.value for profession in ProfessionModel}
    json_data = json.dumps(enum_dict, indent=2)
    return json_data
