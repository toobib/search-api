import math

from fastapi import APIRouter, Query
import requests
from sqlalchemy import or_, asc, select, func
from geoalchemy2 import Geography, functions as geofunc
from starlette import status
from starlette.requests import Request
from stop_words import get_stop_words

from models.materialized_view import OrganisationModel
from utils.db import db_dependency
from utils.utils import fetch_specialties_by_code_ref, str_normalization
from utils.redis import cache_response

router = APIRouter()


@router.get("/search_organisation", status_code=status.HTTP_200_OK)
@cache_response()
async def get_organisation(
        db: db_dependency,
        request: Request,
        raison_sociale: str = None,
        range_around: int = None,
        adresse: str = None,
        latitude: float = None, longitude: float = None,
        order_by_distance: bool = False,
        organisation: str = Query(default = None, description = "Fill in as follow {type}|{id} type can be 'finess' or 'its' for id_technique_structure. example: 'finess|060780996'"),
        _page: int = 1,
        _count: int = 10
        ):
        query = select(OrganisationModel)
        if raison_sociale:
                raison_sociale = str_normalization(raison_sociale)
                query = query.where(OrganisationModel.raison_sociale_normalise.ilike("%" + raison_sociale + "%"))
        if organisation:
                if organisation.split("|")[0] == "finess":
                        query = query.where(OrganisationModel.finess_et == organisation.split("|")[1])
                elif organisation.split("|")[0] == "its":
                        query = query.where(OrganisationModel.id_technique_structure == organisation.split("|")[1])
        if adresse or (latitude and longitude):
                # Prophylaxis: we set range_around in case it is not in the request but the address relates to a street or house number.
                # in this case we check all Practitioners in the perimeter around the central point of this address.
                # We prioritize the pair latitude-longitude over address but we need to get the latitude and longitude of
                # any addresses with the type housenumber or street. We look for request which have an address and either no
                # lat and lon or not one of both.
                address_type = ""
                if adresse and not (latitude and longitude):
                        ban_response = requests.get("https://api-adresse.data.gouv.fr/search/?q="+ adresse)
                        [longitude, latitude] = ban_response.json()['features'][0]['geometry']['coordinates']
                        address_type = ban_response.json()['features'][0]['properties']['type']
                if address_type != "" and address_type == 'municipality':
                        municipality = ban_response.json()['features'][0]['properties']['label']
                        municipality = str_normalization(municipality)
                        query = query.where(OrganisationModel.commune_normalise.ilike(municipality + "%"))
                else:
                        if not range_around:
                                range_around = 1
                        search_point = geofunc.ST_SetSRID(
                                geofunc.ST_MakePoint(longitude, latitude),
                                4326
                        ).cast(Geography)
                        distance_column = geofunc.ST_Distance(OrganisationModel.geom, search_point).label('distance')
                        query = query.add_columns(distance_column).filter(
                                geofunc.ST_DWithin(
                                        OrganisationModel.geom,
                                        search_point,
                                        int(range_around) * 1000
                                )
                        )
                        if order_by_distance:
                                query = query.order_by(asc(distance_column))
        response = {"entry": []}
        count_query = select(func.count()).select_from(query.subquery())
        response['total'] = db.execute(count_query).scalar()
        last_page = math.ceil(response['total'] / _count)
        url_without_page_param = request.url._url.split("_page=")[0] 
        query_results = db.execute(query.limit(_count).offset((_page-1) * _count)).scalars().fetchall()
        if range_around != None and address_type != "municipality":
          for obj, distance in query_results:
              obj_dict = obj.json()                
              obj_dict['distance'] = distance
              response['entry'].append(obj_dict)
        else:
          response['entry'] = [obj.json() for obj in query_results]
        links = []
        links.append({'relation': 'first', 'url': url_without_page_param + "_page=" + str(1) + "&_count=" + str(_count)})
        links.append({'relation': 'self', 'url': url_without_page_param + "_page=" + str(_page) + "&_count=" + str(_count)})
        links.append({'relation': 'next', 'url' : "" if _page >= last_page else url_without_page_param + "_page=" + str(_page+1) + "&_count=" + str(_count)})
        links.append({'relation': 'previous', 'url' : "" if _page == 1 else (url_without_page_param + "_page=" + str(last_page) + "&_count=" + str(_count) if _page > last_page else url_without_page_param + "_page=" + str(_page-1) + "&_count=" + str(_count))})
        links.append({'relation': 'last', 'url': url_without_page_param + "_page=" + str(last_page) + "&_count=" + str(_count)})
        response['link'] = links
        return response



