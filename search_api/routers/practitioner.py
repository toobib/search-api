import math

from fastapi import APIRouter, Query
import requests
from sqlalchemy import select, func, or_, asc
from geoalchemy2 import Geography, functions as geofunc
from starlette import status
from starlette.requests import Request
from stop_words import get_stop_words

from models.materialized_view import PractitionerModel
from models.enum import ProfessionModel, SpecialityModel
from utils.db import db_dependency
from utils.utils import fetch_specialties_by_code_ref, str_normalization
from utils.redis import cache_response

router = APIRouter()


@router.get("/search_practitioner", status_code=status.HTTP_200_OK)
@cache_response()
async def get_practitioner(
        db: db_dependency,
        request: Request,
        # If we set default = "xx", we have a bad behavior: When we provide no info the default value is used.
        profession: ProfessionModel = Query(default = None, description = "Fill in our practitioner's profession. example: 'infirmier'"),
        specialty: SpecialityModel = Query(default = None, description = "Fill in our practitioner's speciality. example: ''"),
        nom: str = None,
        prenom: str = None,
        range_around: int = None,
        latitude: float = None, longitude: float = None,
        order_by_distance: bool = False,
        toobib: str = None, adresse: str = None,
        # If we set default = "xx", we have a bad behavior: When we provide no info the default value is used.
        organisation: str = Query(default = None, description = "Fill in as follow {type}|{id} type can be 'finess' or 'its' for id_technique_structure. example: 'finess|060780996' or 'its|F060780996'"),
        _page: int = 1,
        _count: int = 10
        ):
        query = select(PractitionerModel)
        query = query.distinct(PractitionerModel.nom, PractitionerModel.prenom, PractitionerModel.commune)
        query = query.order_by(PractitionerModel.nom, PractitionerModel.prenom, PractitionerModel.commune)

        if profession:
                profession = str_normalization(profession.value)
                query = query.filter(PractitionerModel.profession_normalise.ilike(profession))
        if nom:
                nom = str_normalization(nom)
                query = query.filter(PractitionerModel.nom_normalise.ilike("%" + nom + "%"))
        if prenom:
                prenom = str_normalization(prenom)
                query = query.filter(PractitionerModel.prenom_normalise.ilike("%" + prenom + "%"))
        if toobib:
                stop_word = get_stop_words('fr')
                names = toobib.split(' ')
                i = 0
                while i < len(names):
                        if names[i] in stop_word:
                                names.pop(i)
                                i -= 1
                        else:
                                names[i] = str_normalization(names[i])
                        i += 1
                name = " ".join(names)
                query = query.filter(PractitionerModel.nom_complet_normalise.ilike("%" + name + "%"))

        if organisation:
                if organisation.split("|")[0] == "finess":
                        query = query.filter(PractitionerModel.finess == organisation.split("|")[1])
                elif organisation.split("|")[0] == "its":
                        query = query.filter(PractitionerModel.id_technique_structure == organisation.split("|")[1])
        if specialty:
                specialty = str_normalization(specialty.value)
                query = query.filter(PractitionerModel.savoir_faire_normalise.ilike(specialty))
        # We check for any location related info
        if adresse or (latitude and longitude):
                # We prioritize the pair latitude-longitude over address.
                # For any addresses of type housenumber or street we need to get the lat and long
                address_type = ""
                if adresse and not (latitude and longitude):
                        ban_response = requests.get("https://api-adresse.data.gouv.fr/search/?q="+ adresse)
                        [longitude, latitude] = ban_response.json()['features'][0]['geometry']['coordinates']
                        address_type = ban_response.json()['features'][0]['properties']['type']
                if address_type != "" and address_type == 'municipality':
                        municipality = ban_response.json()['features'][0]['properties']['label']
                        municipality = str_normalization(municipality)
                        query = query.filter(PractitionerModel.commune_normalise.ilike("%" + municipality + "%"))
                else:
                        # Prophylaxis: we set range_around in case it is not in the request but the address relates to a street or house number.
                        # in this case we check all Practitioners in the perimeter around the central point of this address.
                        if not range_around:
                                range_around = 1
                        search_point = geofunc.ST_SetSRID(
                                geofunc.ST_MakePoint(longitude, latitude),
                                4326
                        ).cast(Geography)
                        distance_column = geofunc.ST_Distance(PractitionerModel.geom, search_point).label('distance')
                        query = query.add_columns(distance_column).filter(
                                geofunc.ST_DWithin(
                                        PractitionerModel.geom,
                                        search_point,
                                        int(range_around) * 1000
                                )
                        )
                        if order_by_distance:
                                query = query.order_by(asc(distance_column))
        response = {"entry": []}
        count_query = select(func.count()).select_from(query.subquery())
        response['total'] = db.execute(count_query).scalar()
        last_page = math.ceil(response['total'] / _count)
        url_without_page_param = request.url._url.split("_page=")[0] 
        print(query)
        query_results = db.execute(query.limit(_count).offset((_page-1) * _count)).scalars().fetchall()
        if range_around != None and address_type != 'municipality':
          for obj, distance in query_results:
              obj_dict = obj.json()                
              obj_dict['distance'] = distance
              response['entry'].append(obj_dict)
        else:
          response['entry'] = [obj.json() for obj in query_results]
        links = []
        links.append({'relation': 'first', 'url': url_without_page_param + "_page=" + str(1) + "&_count=" + str(_count)})
        links.append({'relation': 'self', 'url': url_without_page_param + "_page=" + str(_page) + "&_count=" + str(_count)})
        links.append({'relation': 'next', 'url' : "" if _page >= last_page else url_without_page_param + "_page=" + str(_page+1) + "&_count=" + str(_count)})
        links.append({'relation': 'previous', 'url' : "" if _page == 1 else (url_without_page_param + "_page=" + str(last_page) + "&_count=" + str(_count) if _page > last_page else url_without_page_param + "_page=" + str(_page-1) + "&_count=" + str(_count))})
        links.append({'relation': 'last', 'url': url_without_page_param + "_page=" + str(last_page) + "&_count=" + str(_count)})
        response['link'] = links
        return response



