from enum import Enum
from utils.db import db_dependency, SessionLocal
from sqlalchemy import select, distinct
from models.materialized_view import PractitionerModel
from utils.utils import str_normalization 

def get_pro_normalise_model(db: db_dependency):
    query_professions_normalise = select(distinct(PractitionerModel.profession_normalise))
    result_professions_normalise = db.execute(query_professions_normalise).fetchall()
    return result_professions_normalise 

def get_pro_model(db: db_dependency):
    query_professions = select(distinct(PractitionerModel.profession))
    result_professions = db.execute(query_professions).fetchall()
    return result_professions 

def get_specialite_normalise_model(db: db_dependency):
    query = select(distinct(PractitionerModel.savoir_faire_normalise))
    result = db.execute(query).fetchall()
    return result

def get_specialite_model(db: db_dependency):
    query = select(distinct(PractitionerModel.savoir_faire))
    result = db.execute(query).fetchall()
    return result

professions = []
normalise_professions = []
for i, value in enumerate(get_pro_model(SessionLocal())):
    val = str(value[0])
    val_normalize = str_normalization(val)
    if val_normalize not in normalise_professions and val_normalize != "" and val_normalize != "none":
        professions.append(val)
        normalise_professions.append(str_normalization(val))
professions.sort()

ProfessionModel = Enum("ProfessionModel", [(profession, str_normalization(profession)) for i, profession in enumerate(professions)])

specialities = []
normalise_specialities = []
for i, value in enumerate(get_specialite_model(SessionLocal())):
    val = str(value[0])
    val_normalize = str_normalization(val)
    if val_normalize not in normalise_specialities and val_normalize != "" and val_normalize != "none":
        specialities.append(val)
        normalise_specialities.append(str_normalization(val))
specialities.sort()

SpecialityModel = Enum("SpecialityModel", [(speciality, str_normalization(speciality)) for i, speciality in enumerate(specialities)])
