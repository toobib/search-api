from sqlalchemy import Column, String, Double
from geoalchemy2 import Geography

from utils.db import Base


class PractitionerModel(Base):
    __tablename__ = 'practitioner'

    id_composite = Column(String, primary_key=True)
    inpp = Column(String)
    nom = Column(String)
    nom_normalise = Column(String)
    prenom = Column(String)
    prenom_normalise = Column(String)
    nom_complet_normalise = Column(String)
    profession = Column(String)
    profession_normalise = Column(String)
    savoir_faire = Column(String)
    savoir_faire_normalise = Column(String)
    commune = Column(String)
    commune_normalise = Column(String)
    finess = Column(String)
    id_technique_structure = Column(String)
    latitude = Column(Double)
    longitude = Column(Double)
    ban_id = Column(String)
    siret = Column(String)
    geom = Column(Geography)

    def json(self):
        return {
            'inpp': self.inpp,
            'finess': self.finess,
            'id_technique_structure': self.id_technique_structure,
            'siret': self.siret,
            'ban_id': self.ban_id,
            'nom': self.nom,
            'prenom': self.prenom,
            'profession': self.profession,
            'savoir_faire': self.savoir_faire,
            'commune': self.commune,
        }

class OrganisationModel(Base):
    __tablename__ = 'organisation'
    
    id_composite = Column(String, primary_key=True)
    raison_sociale = Column(String, primary_key=True)
    raison_sociale_normalise = Column(String)
    commune = Column(String)
    commune_normalise = Column(String)
    code_postal = Column(String)
    numero_voie = Column(String)
    nom_voie = Column(String)
    latitude = Column(Double)
    longitude = Column(Double)
    finess_et = Column(String)
    id_technique_structure = Column(String)
    ban_id = Column(String)
    siret = Column(String)
    geom = Column(Geography)

    def json(self):
        return {
            'raison_sociale': self.raison_sociale,
            'finess_et': self.finess_et,
            'id_technique_structure': self.id_technique_structure,
            'siret': self.siret,
            'ban_id': self.ban_id,
        }
