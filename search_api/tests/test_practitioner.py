from starlette.testclient import TestClient
from server import app
import pytest

testClient = TestClient(app)

@pytest.mark.anyio
async def test_search(client: TestClient):
    response = await client.get("/search_practitioner?profession=infirmier&adresse=Paris&_page=5&_count=1")
    body = response.json()
    assert "first" == body['link'][0]['relation']
    assert "self" == body['link'][1]['relation']
    assert "next" == body['link'][2]['relation']
    assert "previous" == body['link'][3]['relation']
    assert "last" == body['link'][4]['relation']

@pytest.mark.anyio
async def test_search_marc(client: TestClient):
    response = await client.get("/search_practitioner?profession=infirmier&toobib=Marc&_page=1&_count=1")
    body = response.json()
    assert int == type(body['total'])

@pytest.mark.anyio
async def test_get_all_nurses(client: TestClient):
    response = await client.get("/search_practitioner?profession=infirmier&_page=5&_count=1")
    body = response.json()
    assert 600000 < body['total']

@pytest.mark.anyio
async def test_get_by_finess(client: TestClient):
    response = await client.get("/search_practitioner?order_by_distance=false&organisation=finess%7C060780996&_page=1&_count=1")
    body = response.json()
    assert 600 > body['total']
    assert 300 < body['total']

@pytest.mark.anyio
async def test_get_by_id_technique_structure(client: TestClient):
    response = await client.get("/search_practitioner?order_by_distance=false&organisation=its%7CF350000147&_page=1&_count=1")
    body = response.json()
    assert 800 > body['total']
    assert 300 < body['total']

@pytest.mark.anyio
async def test_get_ban_id(client: TestClient):
    response = await client.get("https://search.dev.api.toobib.org/search_practitioner?order_by_distance=false&organisation=its%7CR10100000625681&_page=1&_count=10")
    body = response.json()
    assert "75115_5209_00064" == body['entry'][0]['ban_id']

@pytest.mark.anyio
async def test_get_by_profession_and_specialty(client: TestClient):
    response = await client.get("https://search.dev.api.toobib.org/search_practitioner?profession=medecin&specialty=chirurgiethoraciqueetcardiovasculaire&order_by_distance=false&_page=1&_count=10")
    body = response.json()
    assert 1000 < body['total']

@pytest.mark.anyio
async def test_practitioner_must_be_filled(client: TestClient):
    response = await client.get("https://search.dev.api.toobib.org/search_practitioner?nom=RYSER&prenom=Julien&order_by_distance=false&organisation=its%7CR10100000705359&_page=1&_count=10")
    body = response.json()
    assert "810008668609" == body['entry'][0]['inpp']
    assert "R10100000705359" == body['entry'][0]['id_technique_structure']
    assert "53539871300011" == body['entry'][0]['siret']
    
