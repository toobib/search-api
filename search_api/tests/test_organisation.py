from starlette.testclient import TestClient
from server import app
import pytest

testClient = TestClient(app)

@pytest.mark.anyio
async def test_type_total(client: TestClient):
    response = await client.get("/search_organisation?_page=1&_count=1")
    body = response.json()
    assert int == type(body['total'])

@pytest.mark.anyio
async def test_search_hospital(client: TestClient):
    response = await client.get("/search_organisation?raison_sociale=hospital&order_by_distance=false&_page=1&_count=12")
    body = response.json()
    assert "first" == body['link'][0]['relation']
    assert "self" == body['link'][1]['relation']
    assert "next" == body['link'][2]['relation']
    assert "previous" == body['link'][3]['relation']
    assert "last" == body['link'][4]['relation']
    assert "http://test/search_organisation?raison_sociale=hospital&order_by_distance=false&_page=1&_count=12" == body['link'][0]['url']
    assert "http://test/search_organisation?raison_sociale=hospital&order_by_distance=false&_page=1&_count=12" == body['link'][1]['url']
    assert "http://test/search_organisation?raison_sociale=hospital&order_by_distance=false&_page=2&_count=12" == body['link'][2]['url']
    assert "" == body['link'][3]['url']
    assert "http://test/search_organisation?raison_sociale=hospital&order_by_distance=false&_page=76&_count=12" == body['link'][4]['url']
    assert 720 < body['total']

@pytest.mark.anyio
async def test_search_specific_structure(client: TestClient):
    response = await client.get("/search_organisation?raison_sociale=hospital&order_by_distance=false&organisation=its%7CR10100000259830&_page=1&_count=10")
    body = response.json()
    assert "HOSPITAL" == body['entry'][0]['raison_sociale']
    assert None == body['entry'][0]['finess_et']
    assert "R10100000259830" == body['entry'][0]['id_technique_structure']
    assert "03101_1340_00002_bis" == body['entry'][0]['ban_id']

@pytest.mark.anyio
async def test_get_all_organisation(client: TestClient):
    response = await client.get("/search_organisation?_page=5&_count=1")
    body = response.json()
    assert 477000 < body['total']

@pytest.mark.anyio
async def test_get_by_finess(client: TestClient):
    response = await client.get("/search_organisation?order_by_distance=false&organisation=finess%7C060780996&_page=1&_count=10")
    body = response.json()
    assert 1 == body['total']
    assert "CHS SAINTE MARIE" == body['entry'][0]['raison_sociale']
    assert "060780996" == body['entry'][0]['finess_et']
    assert "F060780996" == body['entry'][0]['id_technique_structure']
    assert "77563330800066" == body['entry'][0]['siret']
    assert "06088_3625_00087" == body['entry'][0]['ban_id']
    

@pytest.mark.anyio
async def test_get_by_id_technique_structure(client: TestClient):
    response = await client.get("/search_organisation?order_by_distance=false&organisation=its%7CF350000147&_page=1&_count=10")
    body = response.json()
    assert 1 == body['total']
    assert "GHRE - SITE SAINT MALO BROUSSAIS" == body['entry'][0]['raison_sociale']
    assert "350000147" == body['entry'][0]['finess_et']
    assert "F350000147" == body['entry'][0]['id_technique_structure']
    assert "26350005000012" == body['entry'][0]['siret']
    assert None == body['entry'][0]['ban_id']

@pytest.mark.anyio
async def test_get_rnb(client: TestClient):
    response = await client.get("/search_organisation?order_by_distance=false&organisation=its%7CR10100000625681&_page=1&_count=10")
    body = response.json()
    assert "75115_5209_00064" == body['entry'][0]['ban_id']

@pytest.mark.anyio
async def test_rows_with_identical_raison_sociale(client: TestClient):
    response = await client.get("/search_organisation?raison_sociale=CABINET%20DU%20DR%20ADRIEN%20RIVIERE&order_by_distance=false&_page=1&_count=10")
    body = response.json()
    assert 2 == body['total']
    assert body['total'] == len(body['entry'])

@pytest.mark.anyio
async def test_organisation_must_be_filled(client: TestClient):
    response = await client.get("/search_organisation?order_by_distance=false&organisation=its%7CF590069670&_count=10")
    body = response.json()
    assert 1 == body['total']
    assert "(ASSOCIATION SANTÉ TRAVAIL" == body['entry'][0]['raison_sociale']
    assert "590069670" == body['entry'][0]['finess_et']
    assert "F590069670" == body['entry'][0]['id_technique_structure']
    assert "77562405900645" == body['entry'][0]['siret']
    assert "59350_1769_00199" == body['entry'][0]['ban_id']
    
