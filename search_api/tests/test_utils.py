# # coalesce(multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb), '')
from utils.utils import str_normalization

def test_str_normalization_dash():
    assert "test" == str_normalization("te-st")
    assert "quentinparrot" == str_normalization("quentin-parrot")
    assert "quentinparrot" == str_normalization("-quentin-parrot")
    assert "quentinparrot" == str_normalization("quentinparrot-")
    assert "adrienparrot" == str_normalization("adrien-parrot")

def test_str_normalization_accent():
    assert "test" == str_normalization("té-st")
    assert "quentinparrot" == str_normalization("quéntin-parrot")
    assert "quentinparrot" == str_normalization("-quentin-pàrrot")
    assert "quentinparrot" == str_normalization("quèntinparröt-")

def test_str_normalization_lower():
    assert "test" == str_normalization("té-ST")
    assert "quentinparrot" == str_normalization("quéntin-PARROT")
    assert "quentinparrot" == str_normalization("-quentin-pàrrot")
    assert "queitpar" == str_normalization("QUEITPAR")
    assert "adrienparrot" == str_normalization("ADRIEN PARROT")

def test_str_normalization_mixed():
    assert "simpletest" == str_normalization("  Simple-TEST  ")
    assert "accentremoval" == str_normalization("áccënt-rëmoval")
    assert "spacesanddash" == str_normalization("spaces- and-dash")

def test_str_normalization_strip():
    assert "test" == str_normalization("            té-ST")
    assert "quentinparrot" == str_normalization("quéntin-PARROT                 ")
    assert "quentinparrot" == str_normalization("               -quentin-pàrrot                 ")
    assert "queitpar" == str_normalization("                              QUEITPAR")
    assert "adrienparrot" == str_normalization("ADRIEN PARROT                                ")
    assert "adrienparrot" == str_normalization("ADRIEN PARROT\n                                ")
