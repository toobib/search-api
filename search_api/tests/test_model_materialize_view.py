from models.materialized_view import PractitionerModel, OrganisationModel
from sqlalchemy import select, and_


def test_PractitionerModel_json(db):

    query = select(PractitionerModel).filter(
        and_(
            PractitionerModel.inpp.isnot(None), \
			PractitionerModel.inpp.isnot(None), \
			PractitionerModel.ban_id.isnot(None), \
			PractitionerModel.finess.isnot(None), \
			PractitionerModel.siret.isnot(None), \
			PractitionerModel.id_technique_structure.isnot(None), \
			PractitionerModel.latitude.isnot(None), \
    ))
    response = db.execute(query).scalars().first()
    entity = response.json()
    assert  "" != entity['inpp']
    assert "" != entity['ban_id']
    assert "" != entity['finess']
    assert "" != entity['siret']
    assert "" != entity['id_technique_structure']

def test_OrganisationModel_json(db):
    query = select(OrganisationModel).filter(
        and_(
			OrganisationModel.raison_sociale.isnot(None), \
			OrganisationModel.finess_et.isnot(None), \
			OrganisationModel.id_technique_structure.isnot(None), \
			OrganisationModel.ban_id.isnot(None), \
    ))
    response = db.execute(query).scalars().first()
    entity = response.json()
    assert "" !=  entity['raison_sociale']
    assert "" !=  entity['finess_et']
    assert "" !=  entity['id_technique_structure']
    assert "" !=  entity['ban_id']
