import pytest
from server import app
from httpx import ASGITransport, AsyncClient
from utils.db import SessionLocal

@pytest.fixture(scope="session")
def anyio_backend():
    return "asyncio"

@pytest.fixture(scope="session")
async def client():
    async with AsyncClient(transport=ASGITransport(app=app), base_url="http://test"
    ) as client:
        print("Client is ready")
        yield client

@pytest.fixture(scope="function")
def db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
