from fastapi.testclient import TestClient

from server import app

testClient = TestClient(app)

def test_unit_test():
    assert 1 == 1

def test_hello_world():
    response = testClient.get("/")
    assert response.status_code == 200
    assert response.json()== {"message": 'Hello, explore our documentation at http://testserver/docs'}

