import sys
import subprocess
import uvicorn
from fastapi import FastAPI
import asyncio
from fastapi.middleware.cors import CORSMiddleware


from contextlib import asynccontextmanager
from models.enum import get_specialite_model, get_pro_model


from routers import hello_world, practitioner, organisation, enum
from utils.redis import redis, CacheMiddleware

from utils.db import SessionLocal

origins= [
    'https://dev.front.toobib.org',
    'https://toobib.org',
]

@asynccontextmanager
async def lifespan(app: FastAPI):
    """Ensure Redis connection on app startup."""
    await redis.ping()
    yield
    """Close Redis connection on app shutdown."""
    await redis.close()

app = FastAPI(lifespan = lifespan)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(enum.router)
app.include_router(hello_world.router)
app.include_router(practitioner.router)
app.include_router(organisation.router)

if __name__ == "__main__" :
    if len(sys.argv) == 2 and sys.argv[1] == '--no-test':
        uvicorn.run("server:app", port=5005, host="localhost", reload=True)
    else:
        print("Tests are running...")
        result = subprocess.run(["pytest"], capture_output=True, text=True)
        if result.returncode == 0:
            print("Tests have been run successfully")
            uvicorn.run("server:app", port=5005, host="localhost", reload=True)
        else:
            print(result.stdout)     


