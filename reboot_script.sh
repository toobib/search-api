#!/bin/bash

SESSION_NAME="toobib-searchAPI"
PROJECT_DIR="/home/git/toobib-searchAPI"
FILE_TO_OPEN="server.py"

# Start a new tmux session
tmux new-session -d -s "$SESSION_NAME" -c "$PROJECT_DIR"

# Rename first window
tmux rename-window -t "$SESSION_NAME" "DevOps"

# Run the build and start command in the first pane
tmux send-keys -t "$SESSION_NAME":1 "source .venv/bin/activate" C-m
tmux send-keys -t "$SESSION_NAME":1 "cd search_api" C-m
tmux send-keys -t "$SESSIOM_NAME":1 "python3 server.py" C-m

# Split the first window into two panes
tmux split-window -h -t "$SESSION_NAME" -c "$PROJECT_DIR"

# Create a second window for editing
tmux new-window -t "$SESSION_NAME" -n "Editor" -c "$PROJECT_DIR"

# Open nvim with FILE_TO_OPEN
tmux send-keys -t "$SESSION_NAME":2 "nvim $FILE_TO_OPEN" C-m

# Attach to the session
tmux attach-session -t "$SESSION_NAME"

tmux new-window -t "$SESSION_NAME" -n "DB" -c "$PROJECT_DIR"

tmux send-keys -t "$SESSION_NAME":3 "sudo -u postgres psql" C-m
