FROM python:3.11-alpine

WORKDIR "/app"

COPY ./requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./server.py /app/server.py

COPY .env /app/.env

COPY ./search_api /app/search_api

CMD ["uvicorn", "server:app", "--host", "0.0.0.0", "--port", "5005", "--reload"]
