Usage
============

The production API is currently available here: {{url}} = xxx

The development API is currently available here: {{url}} = [https://search.dev.api.toobib.org/](https://search.dev.api.toobib.org/).

The Swagger is available here: [https://search.dev.api.toobib.org/docs](https://search.dev.api.toobib.org/docs)


## url for the development API:

- endpoints:
  - Hello_world : Test if the API is running
    - GET method : {{url}}/
    - Examples:
      - {{url}}/
  - search_practitioner : Full text search on last name, first name, city, Profession. You can also search for specific last name, first name, city, Position, National identification number (inpp).
    - GET method : {{url}}/search_practitioner
    - Examples:
      - {{url}}/search_practitioner?profession=Infirmier&latitude=46&longitude=4&page_index=1&number_per_page=12
  - search_organisation : Full text search on location.
    - GET method : {{url}}/search_organisation
    - Examples:
      - {{url}}/search_organisation?latitude=46&longitude=4&page_index=1&number_per_page=12
