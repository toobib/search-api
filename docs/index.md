# Documentation

You are reading the documentation for toobib-searchAPI ([toobib-searchAPI: Git repository](https://framagit.org/toobib/search-api)).

## Introduction

The goal of this API is to search for practitioner based on their personal info(name, specialty, workplace ...). This API will be use on the [toobib-website](https://toobib.org/) ([its Git repository](https://framagit.org/toobib/toobib-website)). The FHIR API achieve a similar goal but searching for a practitioner by its location implies long delays (probably due to a join on the Practitioner and Organisation resources). The endpoint organisation is only useful when we are searching for organisations in a specific area.

## API Specs

FastAPI automatically serve an OpenAPI documentation. Our is available here:
[Toobib searchAPI OpenAPI specs](https://search.dev.api.toobib.org/docs)

In the specs you will find a description of the endpoints and of the models.

## Project

```
├── search_api
│   ├── routers          						# API endpoints
│   │   ├── hello_world.py						## test endpoint
│   │   ├── practitioner.py						### endpoints for practionner search
│   │   ├── organisation.py						### endpoints for organisation search
│   │   └── __init__.py      						## needed for import
│   ├── helper           						## application utilities
│   │   ├── auth.py							### implement sso authentification
│   │   ├── utils.py							### implement utility functions
│   │   ├── db.py							### implement db connection
│   │   ├── redis.py							### implement redis connection
│   │   └── __init__.py      						### needed for import
│   ├── config           						## application configuration
│   │   ├── config.py							### add .env management			
│   │   └── __init__.py      						### needed for import
│   ├── tests            						## testing directory
│   │   ├── conftest.py      						### pytest fixture
│   │   ├── test_hello_world.py      					### verify api is launched
│   │   ├── test_utils.py						### test the utility fonctions
│   │   ├── test_organisation.py					### test 'organisation' endpoint
│   │   ├── test_practitioner.py					### test 'practitioner' endpoint
│   │   ├── test_model_materialized_view.py				### test the models
│   │   └── __init__.py      						### 	needed for pytest to work
│   ├── server.py							## start the API 
│   └── __init__.py            						## needed for import
├── db-builder               						# scripts for db installation
│   ├── correction.sh							## clean up csv
│   ├── create-table.sql						## create tables
│   ├── constraints.sql							## Add constraints to tables
│   ├── functions.sql							## load sql functions		
│   ├── load-row-data.sql						## copy from csv to table
│   ├── load-api.sql							## load API table from basic table
│   ├── load-materialized-view-organisation.sql				## load 'organisation' materialized view from table
│   ├── load-materialized-view-practitioner.sql				## load 'practitioner' materialized view from table
│   └── Makefile							## run all command automatically
├── docs               							# markdown project documentation
│   ├── license								## License page
│   │   └── index.md          						### license description
│   └── index.md            						# documentation homepage 
├── LICENSE								# license
├── .gitignore								# Ignore these files during git push
├── .gitlab-ci.yml       					    	# CI/CD configuration
├── Dockerfile								# Dockerfile to run the projet
├── README.md								# describe the project
├── mkdocs.yml           						# documentation configuration
├── pyproject.toml       				    		# project metadata
└── requirements.txt     						# dependencies
```

## Installation

## Getting Started

This section will guide you through the setup process to get the project up and running on your local machine.

## Installation

### Virtual environment and dependencies

Before you start, make sure you have Python and pipx installed on your machine. 

```bash
sudo apt-get install python3.13 pipx
```

Then, install the required libraries by running the following commands:

```bash
python3 -m venv .venv
source .venv/bin/activate
```

Install [poetry](https://python-poetry.org/docs/)

```
pipx install poetry
```

Install all the required python packages:

```
poetry install
```

### .env file Configuration

Before running the repository, create a .env file in '/photo_api' and add the following configurations (adapt the values to your configuration):

```
POSTGRESSQL_DATABASE_URL=postgresql://user:pwd@host/db
REDIS_URL=redis://localhost:6379
CACHE_TIME=600
```


we install uvloop and httptools because they replace the [default asyncio event loop and HTTP parser and make the faster](https://github.com/Kludex/fastapi-tips).

## Running the Project

### start the redis container

```bash
docker run -d -p 6379:6379 --name redis-search-api-container redis
```

### In your terminal

```bash

source .venv/bin/activate // if not already activated
cd photo_api
python3 server.py
```

You can also run the server without testing:

```bash
python3 server.py --no-test
```

### In docker

first build the image

```commandline
sudo docker build -t search-api .
```

then run it:

```commandline
sudo docker run -d --network=host --name search-api-container -p 5001:5001 search-api 
```

## Serve locally the documentation

In order to test the documentation on your local machine, run the following command in the virtual environment:

```bash
mkdocs serve
```

then head to the url: localhost:8000

