/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 17-November-2024

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

DROP index
TRUNCATE TABLE

*************************/

DROP MATERIALIZED VIEW IF EXISTS practitioner;
--- TODO: doc gestion des ban_key 

/************************

FILL API TABLES

*************************/


--- practitioner
--- CREATE MATERIAZED VIEWS AS avec index
CREATE MATERIALIZED VIEW practitioner AS
	(SELECT  
	concat(coalesce(multi_replace(lower(public.unaccent(p.prenom)), '{" ": "", "-":""}'::jsonb), ''), ' ', coalesce(multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb), ''), p.libelle_commune, p.identification_nationale_pp, ban.id)			AS id_composite
  ,  p.identification_nationale_pp 											AS inpp
	, p.nom														AS nom
	, coalesce(multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb), '')			AS nom_normalise
	, p.prenom													AS prenom
	, coalesce(multi_replace(lower(public.unaccent(p.prenom)), '{" ": "", "-":""}'::jsonb), '')			AS prenom_normalise
	, concat(coalesce(multi_replace(lower(public.unaccent(p.prenom)), '{" ": "", "-":""}'::jsonb), ''), ' ', coalesce(multi_replace(lower(public.unaccent(p.nom)), '{" ": "", "-":""}'::jsonb), ''))			AS nom_complet_normalise
	, p.libelle_profession												AS profession
	, coalesce(multi_replace(lower(public.unaccent(p.libelle_profession)), '{" ": "", "-":""}'::jsonb), '')		AS profession_normalise
	, p.code_savoirfaire
	, p.libelle_savoirfaire												AS savoir_faire
	, coalesce(multi_replace(lower(public.unaccent(p.libelle_savoirfaire)), '{" ": "", "-":""}'::jsonb), '')	AS savoir_faire_normalise
	, p.libelle_commune												AS commune
	, coalesce(multi_replace(lower(public.unaccent(p.libelle_commune)), '{" ": "", "-":""}'::jsonb), '')		AS commune_normalise
	, ban.lat AS latitude
 	, ban.lon AS longitude
 	, ban.id AS ban_id
 	, ban.geom AS geom
 	, ftb.finess_et AS finess
  , CASE 
      WHEN p.siret IS NOT NULL THEN p.siret
      WHEN its.rpps_siret IS NOT NULL THEN its.rpps_siret  
      ELSE ftb.finess_siret 
    END AS siret
	, p.id_technique_structure AS id_technique_structure
	FROM ps_libreacces_personne_activite p
  LEFT JOIN referentiel_finess rf ON p.finess = rf.finess_et
	LEFT JOIN finess_to_ban ftb ON ftb.finess_et = rf.finess
	LEFT JOIN its_to_ban its ON its.rpps_id_technique_structure = p.id_technique_structure
  LEFT JOIN ban_adresses_france ban ON ban.id =
			CASE 
				WHEN p.finess IS NOT NULL THEN ftb.ban_key  
				WHEN p.id_technique_structure IS NOT NULL THEN its.ban_key
			END  
	);

/************************

Create Index

*************************/

CREATE INDEX idx_practitioner_inpp ON practitioner  (inpp ASC);
CREATE INDEX idx_practitioner_prenom_normalise ON practitioner USING GIN (prenom_normalise public.gin_trgm_ops);
CREATE INDEX idx_practitioner_nom_normalise ON practitioner USING GIN (nom_normalise public.gin_trgm_ops);
CREATE INDEX idx_practitioner_nom_nom_complet_normalise ON practitioner USING GIN (nom_complet_normalise public.gin_trgm_ops);
CREATE INDEX idx_practitioner_profession_normalise ON practitioner  USING GIN (profession_normalise public.gin_trgm_ops);
CREATE INDEX idx_practitioner_savoir_faire_normalise ON practitioner USING GIN (savoir_faire_normalise public.gin_trgm_ops);
CREATE INDEX idx_practitioner_commune_normalise ON practitioner USING GIN (commune_normalise public.gin_trgm_ops);
-- CREATE INDEX idx_practitioner_profession ON practitioner  (libelle_profession ASC);
