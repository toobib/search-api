/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ###### 
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ###### 
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ###### 

last revised: 04-Dec-2024

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

Profesionnels de sante Libre Access

 ************************/

DROP TABLE IF EXISTS ps_libreacces_personne_activite CASCADE;
DROP TABLE IF EXISTS ban_adresses_france CASCADE;
DROP TABLE IF EXISTS ban_adresses_france_tmp CASCADE;

DROP TABLE IF EXISTS finess_to_ban CASCADE;
DROP TABLE IF EXISTS its_to_ban CASCADE;

DROP TABLE IF EXISTS referentiel_finess CASCADE;

CREATE TABLE ps_libreacces_personne_activite (
	type_identifiant_PP 				    TEXT	NULL,
	identifiant_PP 				 	        TEXT	NULL,
	identification_nationale_PP 			TEXT	NULL,
	code_civilite_exercice 				    TEXT	NULL,
	libelle_civilite_exercice 			    TEXT	NULL,
	code_civilite 					        TEXT	NULL,
	libelle_civilite 				        TEXT	NULL,
	nom						                TEXT	NULL,
	prenom						            TEXT	NULL,
	code_profession 				        TEXT	NULL,
	libelle_profession 				        TEXT	NULL,
	code_categorie_professionnelle 			TEXT	NULL,
	libelle_categorie_professionnelle 		TEXT	NULL,
	code_type_savoirfaire 				    TEXT	NULL,
	libelle_type_savoirfaire  			    TEXT	NULL,
	code_savoirfaire 				        TEXT	NULL,
	libelle_savoirfaire  				    TEXT	NULL,
	code_mode_exercice 				        TEXT	NULL,
	libelle_mode_exercice 				    TEXT	NULL,
	siret 						            TEXT	NULL,
	siren						            TEXT	NULL,
	finess 						            TEXT	NULL,
	finess_etablissement_juridique 			TEXT	NULL,
	id_technique_structure 				    TEXT	NULL,
	raison_sociale 					        TEXT	NULL,
	enseigne_com  					        TEXT	NULL,
	complement_destinataire  			    TEXT	NULL,
	complement_point_geographique  			TEXT	NULL,
	numero_voie           				    TEXT	NULL,
	indice_repetition_voie    			    TEXT	NULL,
	code_type_de_voie 				        TEXT	NULL,
	libelle_type_de_voie				    TEXT	NULL,
	libelle_voie					        TEXT	NULL,
	mention_distribution				    TEXT	NULL,
	bureau_cedex					        TEXT	NULL,
	code_postal					            TEXT	NULL,
	code_commune					        TEXT	NULL,
	libelle_commune					        TEXT	NULL,
	code_pays					            TEXT	NULL,
	libelle_pays					        TEXT	NULL,
	telephone					            TEXT	NULL,
	telephone_2					            TEXT	NULL,
	telecopie					            TEXT	NULL,
	email						            TEXT	NULL,
	code_departement				        TEXT	NULL,
	libelle_departement				        TEXT	NULL,
	ancien_id_structure				        TEXT	NULL,
	autorite_enregistrement				    TEXT	NULL,
	code_secteur_activite				    TEXT	NULL,
	libelle_secteur_activite			    TEXT	NULL,
	code_section_tableau_pharmaciens		TEXT	NULL,
	libelle_section_tableau_pharmaciens		TEXT	NULL
)
;

CREATE TABLE ban_adresses_france (
    id                                      TEXT                            NULL,
    id_fantoir                              TEXT    						NULL,
    numero                                  INTEGER 						NULL,
    rep                                     TEXT    						NULL,
    nom_voie                                TEXT    						NULL,
    code_postal                             INTEGER 						NULL,
    code_insee                              TEXT    						NULL,
    nom_commune                             TEXT    						NULL,
    code_insee_ancienne_commune             TEXT    						NULL,
    nom_ancienne_commune                    TEXT    						NULL,
    x                                       DOUBLE PRECISION		        NULL,
    y                                       DOUBLE PRECISION                NULL,
    lon                                     DOUBLE PRECISION                NULL,
    lat                                     DOUBLE PRECISION                NULL,
    type_position                           TEXT    						NULL,
    alias                                   TEXT    						NULL,
    nom_ld                                  TEXT    						NULL,
    libelle_acheminement                    TEXT    						NULL,
    nom_afnor                               TEXT    						NULL,
    source_position                         TEXT    						NULL,
    source_nom_voie                         TEXT    						NULL,
    certification_commune                   INTEGER 						NULL,
    cad_parcelles                           TEXT    						NULL,
    geom                                    GEOGRAPHY(Point, 4326)
);

CREATE TABLE ban_adresses_france_tmp (
    id                                      TEXT                            NULL,
    id_fantoir                              TEXT    						NULL,
    numero                                  INTEGER 						NULL,
    rep                                     TEXT    						NULL,
    nom_voie                                TEXT    						NULL,
    code_postal                             INTEGER 						NULL,
    code_insee                              TEXT    						NULL,
    nom_commune                             TEXT    						NULL,
    code_insee_ancienne_commune             TEXT    						NULL,
    nom_ancienne_commune                    TEXT    						NULL,
    x                                       DOUBLE PRECISION                NULL,
    y                                       DOUBLE PRECISION                NULL,
    lon                                     DOUBLE PRECISION                NULL,
    lat                                     DOUBLE PRECISION                NULL,
    type_position                           TEXT    						NULL,
    alias                                   TEXT    						NULL,
    nom_ld                                  TEXT    						NULL,
    libelle_acheminement                    TEXT    						NULL,
    nom_afnor                               TEXT    						NULL,
    source_position                         TEXT    						NULL,
    source_nom_voie                         TEXT    						NULL,
    certification_commune                   INTEGER 						NULL,
    cad_parcelles                           TEXT    						NULL
);

CREATE TABLE its_to_ban (
    rpps_id_technique_structure             TEXT                            NULL,
    rpps_siret                              TEXT    						NULL,
    ban_id                                  TEXT 		    				NULL,
    ban_key                                 TEXT    						NULL,
    rpps_ban_algo_score                     TEXT     						NULL,
    rpps_ban_algo_uuid                      TEXT    						NULL
);

CREATE TABLE finess_to_ban (
	finess_et			        TEXT		NULL,
	finess_siret				TEXT		NULL,
	ban_id					TEXT		NULL,
	ban_key                 	        TEXT       	NULL,
	finess_ban_algo_score			FLOAT		NULL,
	finess_ban_algo_uuid			UUID		NULL
);

CREATE TABLE referentiel_finess (
  finess	  TEXT NULL,
  statut	  TEXT NULL,
  finess_et TEXT NULL
);


