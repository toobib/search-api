/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 04-Feb-2021

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

Profesionnels de sante Libre Access

************************/

TRUNCATE TABLE ps_libreacces_personne_activite CASCADE;
DROP INDEX IF EXISTS idx_personne_activite_id_nat_pp;
DROP INDEX IF EXISTS idx_personne_activite_profession;
DROP INDEX IF EXISTS idx_personne_activite_categorie_professionnelle;
DROP INDEX IF EXISTS idx_personne_activite_savoirfaire;
DROP INDEX IF EXISTS idx_personne_activite_finess;
DROP INDEX IF EXISTS idx_personne_activite_code_commune;
DROP INDEX IF EXISTS idx_personne_activite_code_postal;
DROP INDEX IF EXISTS xpk_finess_to_ban_table;
DROP INDEX IF EXISTS xpk_ban_adresses_france_table;

TRUNCATE TABLE ban_adresses_france_tmp CASCADE;
TRUNCATE TABLE ban_adresses_france CASCADE;
DROP INDEX IF EXISTS idx_ban_addresses_france;

TRUNCATE TABLE finess_to_ban CASCADE;
TRUNCATE TABLE its_to_ban CASCADE;

TRUNCATE TABLE referentiel_finess CASCADE;

\copy ps_libreacces_personne_activite FROM 'download/PS_LibreAcces_Personne_activite.csv' WITH DELIMITER '|' CSV HEADER ;
\copy ban_adresses_france_tmp FROM 'download/adresses-france.csv' WITH DELIMITER ';' CSV HEADER ;
\copy its_to_ban FROM 'download/its-to-ban.csv' WITH DELIMITER ',' CSV HEADER ;
\copy finess_to_ban FROM 'download/finess-to-ban.csv' WITH DELIMITER ',' CSV HEADER ;
\copy referentiel_finess FROM 'download/referentiel-finess.csv' WITH DELIMITER '|' CSV HEADER;

INSERT INTO ban_adresses_france SELECT DISTINCT ON (id) * FROM ban_adresses_france_tmp ORDER BY id, (lat + lon) DESC;
DROP TABLE ban_adresses_france_tmp;
VACUUM FULL ban_adresses_france;
UPDATE ban_adresses_france  SET geom = ST_SetSRID(ST_MakePoint(lon, lat), 4326);



/************************

Create Index

************************/

CREATE INDEX idx_personne_activite_id_nat_pp  ON  ps_libreacces_personne_activite (identification_nationale_PP ASC);
CLUSTER ps_libreacces_personne_activite  USING idx_personne_activite_id_nat_pp ;
-- CREATE INDEX idx_personne_activite_code_commune;
CREATE INDEX idx_personne_activite_finess  ON  ps_libreacces_personne_activite (finess ASC);
CREATE INDEX idx_personne_activite_code_commune  ON  ps_libreacces_personne_activite (code_commune ASC);
CREATE INDEX idx_personne_activite_code_postal  ON  ps_libreacces_personne_activite (code_postal ASC);
CREATE INDEX idx_personne_activite_profession  ON  ps_libreacces_personne_activite (libelle_profession ASC);
CREATE INDEX idx_personne_activite_categorie_professionnelle  ON  ps_libreacces_personne_activite (libelle_categorie_professionnelle ASC);
CREATE INDEX idx_personne_activite_savoirfaire  ON  ps_libreacces_personne_activite (libelle_savoirfaire ASC);

--------------------
-- finess_to_ban --
--------------------

CREATE INDEX idx_finess_to_ban  ON  finess_to_ban (finess_et ASC);
CLUSTER finess_to_ban USING idx_finess_to_ban;

--------------------
-- ban_adresses_france --
--------------------

-- CREATE INDEX idx_ban_adresses_france  ON  ban_adresses_france (id ASC);
-- CLUSTER ban_adresses_france USING idx_ban_adresses_france;
