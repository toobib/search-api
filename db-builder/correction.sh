echo "Dipl_Aut_Exerc_ in progress..."
cut -d '|' -f 1-13 download/PS_LibreAcces_Dipl_AutExerc_* >download/PS_LibreAcces_Dipl_AutExerc.csv

echo "Personne_activite_ in progress..."
cut -d '|' -f 1-52 download/PS_LibreAcces_Personne_activite_* >download/PS_LibreAcces_Personne_activite.csv
sed -i "s/CHIRURGIE ORALE/Chirurgie orale/g" download/PS_LibreAcces_Personne_activite.csv
sed -i "s/Chirurgie Orale/Chirurgie orale/g" download/PS_LibreAcces_Personne_activite.csv
# macOS
# sed -i "" "s/CHIRURGIE ORALE/Chirurgie Orale/g" download/PS_LibreAcces_Personne_activite.csv
# sed -i "" "s/Chirurgie Orale/Chirurgie orale/g" download/PS_LibreAcces_Personne_activite.csv

echo "finess in progress..."
sed 's/","/|/g' download/referentiel-finess.txt >output.csv
cut -d '|' -f 3,5,11 output.csv >download/referentiel-finess.csv
rm output.csv
