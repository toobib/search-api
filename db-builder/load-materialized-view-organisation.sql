/*********************************************************************************
#
#
#     Licence
#
#
#
 ********************************************************************************/

/************************

####### #######  ####### ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # ######     #    ######
   #    #     #  #     # #     #    #    #     #
   #    #     #  #     # #     #    #    #     #
   #    #######  ####### ######     #    ######

last revised: 17-November-2024

Authors:  Adrien PARROT
Authors:  Quentin PARROT

 *************************/

/************************

DROP index
TRUNCATE TABLE

*************************/

DROP MATERIALIZED VIEW IF EXISTS organisation;
--- TODO: doc gestion des ban_key 

/************************

FILL API TABLES

*************************/


--- recherche
--- CREATE MATERIAZED VIEWS AS avec index
CREATE MATERIALIZED VIEW organisation AS
SELECT * FROM	(SELECT 
	CONCAT(coalesce(multi_replace(lower(public.unaccent(p.raison_sociale)), '{" ": "", "-":""}'::jsonb), ''), ban.nom_commune, ban.code_postal, ban.nom_voie, ban.numero)	AS id_composite
	, p.raison_sociale											AS raison_sociale
	, coalesce(multi_replace(lower(public.unaccent(p.raison_sociale)), '{" ": "", "-":""}'::jsonb), '')	AS raison_sociale_normalise
	, ban.nom_commune											AS commune
	, coalesce(multi_replace(lower(public.unaccent(ban.nom_commune)), '{" ": "", "-":""}'::jsonb), '')	AS commune_normalise
	, ban.code_postal 											AS code_postal
	, ban.numero												AS numero_voie
	, ban.nom_voie												AS nom_voie
	, ban.lat 												AS latitude	
   	, ban.lon 												AS longitude
   	, ban.id 												AS ban_id
   	, ban.geom 												AS geom
   	, p.finess 												AS finess_et
   	, p.siret 												AS siret
	, p.id_technique_structure 										AS id_technique_structure
	, ROW_NUMBER() OVER (PARTITION BY p.raison_sociale, ban.numero, ban.nom_voie ORDER BY ban.geom)
	FROM ps_libreacces_personne_activite p
	LEFT JOIN finess_to_ban fbr ON fbr.finess_et = p.finess
	LEFT JOIN its_to_ban its ON its.rpps_id_technique_structure = p.id_technique_structure
    	LEFT JOIN ban_adresses_france ban ON ban.id =
			CASE 
				WHEN p.finess IS NOT NULL THEN fbr.ban_key  
				WHEN p.id_technique_structure IS NOT NULL THEN its.ban_key
			END  
	WHERE (p.finess IS NOT NULL OR p.id_technique_structure IS NOT NULL)
	) subquery WHERE row_number = 1;

/************************

Create Index

*************************/

CREATE INDEX idx_organisation_id ON organisation  (id_composite ASC);
CREATE INDEX idx_organisation_raison_sociale_normalise ON organisation USING GIN (raison_sociale_normalise public.gin_trgm_ops);
CREATE INDEX idx_organisation_commune_normalise ON organisation USING GIN (commune_normalise public.gin_trgm_ops);
