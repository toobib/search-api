Introduction
============

This API is part of toobib.org projet. 
It will be served on server provided by [interhop.org](interorg.org)


## Data sources

- [annuaire.sante.fr](https://annuaire.sante.fr/web/site-pro)
> Près de 200.000 professionnels ADELI, incluant des acteurs clés du médico-social, ont récemment été intégrés dans l’Annuaire Santé, et donc intégrés dans les extractions : cela concerne les psychologues, assistants de service social, ostéopathes, psychothérapeutes, chiropracteurs, assistants dentaires, et les physiciens médicaux.
There is RPPS number (unique professional number). This dataset is our reference.
- [Annuaire santé de la Cnam](https://www.data.gouv.fr/fr/datasets/annuaire-sante-de-la-cnam/#_)
> Comme expliqué, les données sont issues de traitements statistiques sur l’activité des professionnels. Avec des règles de seuils et de filtres. Les actes non pratiqués ou en deçà du seuil de significativité sur la période considérée ne produisent donc aucune activité pour le professionnel en question sur cette période.
Look to dirty to use the source
Other links [Annuaire Santé : Liste, localisation et tarifs des professionnels de santé ](https://public.opendatasoft.com/explore/dataset/medecins/information/?dataChart=eyJxdWVyaWVzIjpbeyJjb25maWciOnsiZGF0YXNldCI6Im1lZGVjaW5zIiwib3B0aW9ucyI6eyJxIjoiVEFIQVIgYWxiZXJ0In19LCJjaGFydHMiOlt7ImFsaWduTW9udGgiOnRydWUsInR5cGUiOiJjb2x1bW4iLCJmdW5jIjoiQVZHIiwieUF4aXMiOiJjb2x1bW5fMTEiLCJzY2llbnRpZmljRGlzcGxheSI6dHJ1ZSwiY29sb3IiOiIjRkY1MTVBIn1dLCJ4QXhpcyI6ImNpdmlsaXRlIiwibWF4cG9pbnRzIjo1MCwic29ydCI6IiJ9XSwidGltZXNjYWxlIjoiIiwiZGlzcGxheUxlZ2VuZCI6dHJ1ZSwiYWxpZ25Nb250aCI6dHJ1ZX0%3D&location=2,30.51163,0&basemap=jawg.streets)
- [Base officielle des codes postaux ](https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/information/?disjunctive.code_commune_insee&disjunctive.nom_de_la_commune&disjunctive.code_postal&disjunctive.ligne_5)
From "La Poste"
Useful to get latitude and longitude
- [FINESS Extraction du Fichier des établissements](https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/#_)
List of establishments in the health and social field.


Usage
============

The production API is currently available here: {{url}} = xxx

The development API is currently available here: {{url}} = [https://search.dev.api.toobib.org/](https://search.dev.api.toobib.org/).

The Swagger is available here: [https://search.dev.api.toobib.org/docs](https://search.dev.api.toobib.org/docs)


## url for the development API:

- endpoints:
  - Hello_world : Test if the API is running
    - GET method : {{url}}/
    - Examples:
      - {{url}}/
  - hi_marc : Test the DB connection
    - GET method : {{url}}/hi_marc
    - Examples:
      - {{url}}/hi_marc
  - search : Full text search on last name, first name, city, Profession. You can also search for specific last name, first name, city, Position, National identification number (inpp).
    - GET method : {{url}}/search
    - Examples:
      - {{url}}/search?profession=Infirmier&latitude=46&longitude=4&page_index=1&number_per_page=12


Installation
===============

#### Database
- Installation postgreSQL and postgis
```bash
sudo apt-get install postgres postgresql-14-postgis-3
```

- connect to postgres
```bash
sudo -u postgres psql
```

- Create user and database
```psql
CREATE USER "USER" WITH PASSWORD 'PWD' CREATEDB;
CREATE DATABASE "DB" OWNER "USER";
```

- Install the extensions pg_trgm and unaccent:
```psql
\c "DB"
CREATE EXTENSION unaccent;
CREATE EXTENSION pg_trgm;
CREATE EXTENSION postgis;
```

- Exit psql and edit macros USER, DB in Makefile

- create a [Config](https://wiki.postgresql.org/wiki/Pgpass) .pgpass file in your home. In it write the following

```bash
"IP":"PORT":"DB":"USER":"PWD"
```

example: 
```bash
localhost:5432:toobib:toobib:password
```
- apply the correct right to .pgpass file
```bash
sudo chmod 600 ~/.pgpass
```

- Run Makefile
```bash
make all
```

if your db is empty after the make, you might need to replace, in the file ./db-builder/annuaire_sante_load_api.sql, 'public.unaccent' by 'unaccent'.

The ban database has multiple rows with the same id. We keep the one with the greater (lat + lon). 


#### Run Api Server

- create .env file in the search_api folder. Within this document you will store the variable specific to your project:
```bash
vim .env
```

- paste this code in the file .env file
```dotenv
POSTGRESQL_DATABASE_URL=postgresql://USER:PWD@localhost/DB
```

Be sure that general dependencies have been installed by running this command in the root folder:

``` bash
sudo apt-get install python3-pip python3-psycopg2 libpq-dev
```  

 Install virtual env
```bash
apt install python3.10-venv
```
Create the venv 
```bash
python3 -m venv .venv
```

Add the pip dependencies
```bash
source .venv/bin/activate
pip3 install -r requirements.txt
```

```bash
python3 server.py
```


# docker

## Docker container for redis

You need to start the docker container for redis

```commandline
docker run -d -p 6379:6379 --name redis-searchAPI redis
```

##
first build the image

```commandline
sudo docker build -t search-api .
```

then run it:

```commandline
sudo docker run -d --network=host --name seach-api-container -p 5005:5005 search-api 
```


Interesting resources
============

#### Make
- https://software-carpentry.org/lessons/
  - http://swcarpentry.github.io/make-novice/
- http://www.leeladharan.com/sqlalchemy-query-with-or-and-like-common-filters


#### Full Text Search In Postgres
> The pg_trgm module provides functions and operators for determining the similarity of ASCII alphanumeric text based on trigram matching, as well as index operator classes that support fast searching for similar strings.
> In choosing which index type to use, GiST or GIN, consider these performance differences:
- GIN index lookups are about three times faster than GiST
- GIN indexes take about three times longer to build than GiST
- GIN indexes are moderately slower to update than GiST indexes, but about 10 times slower if fast-update support was disabled [...]
- GIN indexes are two-to-three times larger than GiST indexes

- https://niallburkley.com/blog/index-columns-for-like-in-postgres/
- https://www.postgresql.org/docs/9.1/textsearch-indexes.html


##### Links

1. [annuaire.sante.fr](https://annuaire.sante.fr/web/site-pro)
- [Annuaire SantéNouvelles extractions en libre accès](https://esante.gouv.fr/sites/default/files/media_entity/documents/ANS_Description-des-nouvelles-extractions-en-libre-acces_V1.2.pdf)
- [Dossier des Spécifications Fonctionnelles et Techniques](https://esante.gouv.fr/sites/default/files/media_entity/documents/Annuaire_sante_fr_DSFT_Extractions_donnees_libre%20acc%C3%A8s_V2.2.2.pdf)
- [Extraction annuaire - Quality check](https://pad.interhop.org/s/HyeeUgt1v#)

2. [Annuaire santé de la Cnam](https://www.data.gouv.fr/fr/datasets/annuaire-sante-de-la-cnam/#_)
- [Open Data for Health](https://public.opendatasoft.com/explore/?sort=modified&q=annuaire&refine.theme=Sant%C3%A9)
>>>>>>> origin/fusionnerWithServeurData
  - [Nomenclature Professions du domaine de la santé](https://public.opendatasoft.com/explore/dataset/nomenclature-professions-medecine/table/)
  - [Annuaire et localisation des professionnels de santé ](https://public.opendatasoft.com/explore/dataset/annuaire-des-professionnels-de-sante/table/)
  - [Annuaire Santé : Liste, localisation et tarifs des professionnels de santé ](https://public.opendatasoft.com/explore/dataset/medecins/table/)

3. [Base officielle des codes postaux ](https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/information/?disjunctive.code_commune_insee&disjunctive.nom_de_la_commune&disjunctive.code_postal&disjunctive.ligne_5)

4. [FINESS : Liste des établissements du domaine sanitaire et social.](https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/#_)
- DOC Structure: Établissement (ET) Structure & Géolocalisation
- DOC Structure: Établissement (ET) Structure & Géolocalisation
- [Definitions](http://finess.sante.gouv.fr/fininter/jsp/definitions.do?codeDefinition=2)
- [Search](http://finess.sante.gouv.fr/fininter/jsp/actionRechercheSimple.do)
- [Definition : RPPS -  AM / FINESS](https://www.ameli.fr/fileadmin/user_upload/documents/memo_RPPS.pdf)

5. [Base Sirene v3](https://public.opendatasoft.com/explore/dataset/sirene_v3/information/?disjunctive.libellecommuneetablissement&disjunctive.etatadministratifetablissement&disjunctive.sectionetablissement&disjunctive.naturejuridiqueunitelegale&sort=datederniertraitementetablissement&q=44261444200220)
- [Numéro SIREN, numéro SIRET, quelle différence ?](https://www.economie.gouv.fr/entreprises/numeros-siren-siret)
